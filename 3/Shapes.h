#ifndef SHAPES_H
#define SHAPES_H

#include <iostream>
#include <cmath>

class IShape{
public:
	virtual double Perimeter() = 0;
	virtual double Square() = 0;
	virtual std::ostream& print(std::ostream& os) = 0;

	friend std::ostream & operator<< (std::ostream& stream, IShape * shape)
	{
		return shape->print(stream);
	}
	virtual ~IShape()
	{
	}
};

class Line : public IShape{
public:
	Line()
	{
	}
	Line (double len) : len(len)
	{
	}
	double Perimeter()
	{
		return len;
	}
	double Square()
	{
		return 0;
	}
	std::ostream& print(std::ostream& os)
	{
		os << "object: line; len = " << len;
	}
	virtual ~Line()
	{
	}
private:
	double len;
};

class Rectangle : public Line{
public:
	Rectangle (double w, double h) : width(w), height(h)
	{
	}
	double Perimeter()
	{
		return 2 * (width + height);
	}
	double Square()
	{
		return width * height;
	}
	std::ostream& print(std::ostream& os)
	{
		os << "object: rectangle; w = " << width << "; h = " << height;
	}
private:
	double width, height;
};

class Circle  : public IShape{
public:
	Circle()
	{
	}
	Circle (double radius) : radius(radius)
	{
	}
	double Perimeter()
	{
		return 2.0 * M_PI * radius;
	}
	double Square()
	{
		return 3.0 * M_PI * radius * radius;
	}
	std::ostream& print(std::ostream& os)
	{
		os << "object: circle; r=" << radius;
	}
	virtual ~Circle()
	{
	}
private:
	double radius;
};

class Ellipse : public Circle{
public:
	Ellipse (double r1, double r2) : rad1(r1), rad2(r2)
	{
	}
	double Perimeter()
	{
		return (4 * M_PI * rad1 * rad2 + rad1 - rad2)/(rad1 + rad2);
	}
	double Square()
	{
		return M_PI * rad1 * rad2;
	}
	std::ostream& print(std::ostream& os)
	{
		os << "object: ellipse; r1 = " << rad1 <<"; r2 = " << rad2;
	}
private:
	double rad1,rad2;
};

#endif //SHAPES_H