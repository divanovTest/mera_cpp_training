#include <iostream>
#include "Shapes.h"

using namespace std;

int main(void)
{
	IShape ** shapes = new IShape*[5];
	shapes[0] = new Line(5);
	shapes[1] = new Rectangle(10, 12);
	shapes[2] = new Circle(8);
	shapes[3] = new Ellipse(4, 3);
	shapes[4] = new Circle(5);

	for (int i = 0; i < 5; i++)
	{
		cout << shapes[i] << endl << "Perimeter: " << shapes[i]->Perimeter() << endl;
		cout << "Square: " << shapes[i]->Square() << endl << endl;
		delete shapes[i];
	}
	delete[] shapes;

	return 0;
}