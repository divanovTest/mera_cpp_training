#ifndef LIST_H
#define LIST_H

#include <iostream>

using namespace std;

template<typename T> struct ListElem{
	T data;
	ListElem *next, *prev;
};

template<typename T> class List{
public:
	List():head(NULL), tail(NULL)
	{
	}

	~List()
	{
		while (head)
	    {
	        tail = head->next;
	        delete head;
	        head = tail;
	    }
	}

	void Add(T elem)
	{
		++count;
		ListElem<T> *temp = new ListElem<T>;
		temp->next = NULL;
		temp->data = elem;
		 
		if (head != NULL)
		{
		    temp->prev = tail;
		    tail->next = temp;
		    tail = temp;
		}
		else
		{
		    temp->prev = NULL;
		    head = tail = temp;
		}
	}

	void Insert(T elem, int pos)
	{
		pos--;
		if(pos > count)
		{
			cout << "Error. List array have " << count << " elements. You try to create new node from " << pos << " position." << endl;
			return;
		}
		if(pos == count || count == 0)
		{
			Add(elem);
			return;
		}
		++count;
		ListElem<T> *temp = new ListElem<T>, *prev = head, *next;
		while(prev != NULL && pos > 0)
		{
			prev = prev->next;
			--pos;
		};
		next = prev->next;
 		temp->prev = prev;
		temp->next = next;
		temp->data = elem;

		prev->next = temp;
		next->prev = temp;
	}

	int Length()
	{
		return count;
	}

	unsigned int Search(T elem)
	{
		if(count <= 0)
		{
			cout << "List is empty." << endl;
			return -1;
		}
		ListElem<T> *temp = head;
		int pos = 0;
		while(temp && temp->data != elem)
		{
			temp = temp->next;
			pos++;
		}
		return pos;
	}

	void Remove(int pos)
	{
		if(count <= 0)
		{
			cout << "List is empty." << endl;
			return;
		}
		if(pos > count)
		{
			cout << "You try to delete not existing element in list array" << endl;
			return;
		}
		ListElem<T> *temp = head, *prev, *next;
		int countpos = pos;
		while(temp != NULL && (countpos--) > 0)
		{
			temp = temp->next;
		}

		prev = temp->prev;
		next = temp->next;

		if (prev != NULL){
			prev->next = temp->next;
		}
		else
		{
			head = next;
		}
		if (next != NULL)
		{
			next->prev = temp->prev;
		}
		else
		{
			tail = prev;
		}
		--count;
	}

	void Print(void)
	{
		ListElem<T> *temp = head;
		int countpos = count; 
		while(temp && (countpos--)>0)
		{
			cout << temp->data << " ";
			temp = temp->next;
		};
		cout << endl;
	}

	ListElem<T>* operator[](int pos)
	{
		if(pos > count || pos < 0)
		{
			cout << "Not existing element" << endl;
			return nullptr;
		}
		ListElem<T> *temp = head;
		while(temp != NULL && (pos--) > 0)
		{
			temp = temp->next;
		};
		return temp;
	}
	ListElem<T> *head, *tail;
private:
	static int count;
};
template<typename T> int List<T>::count = 0;

#endif