#include <iostream>
#include "List.h"

using namespace std;

int main(void)
{
	List<int> list;
	list.Add(1);
	list.Add(3);
	list.Add(5);

	list.Insert(2, 1);
	list.Insert(4, 3);
	
	if (list.Length() != 5) cout<<"bad list\n";
	if (list.Search(4) != 3) cout << "bad list\n";

	list.Remove(0);
	if (list[0]->data != 2) cout << "bad list\n";
	return 0;
}