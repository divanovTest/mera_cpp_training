#ifndef BITARRAY_H
#define BITARRAY_H

#include <vector>
#include <algorithm>
#include <cmath>

/*
void print_vec(std::vector<unsigned int> &vecl)
{
	int temp = 1;
	for_each(vecl.begin(), vecl.end(), [&temp](int n){std::cout << n << (temp % 8 == 0 ? " " : ""); ++temp;});
	std::cout << std::endl;
}

//"+"
void bit_add(std::vector<unsigned int> &vecl, std::vector<unsigned int> &vecr)
{
	unsigned int div = 1;
	int diff = ((unsigned int)vecl.size() - (unsigned int)vecr.size());
	if (diff < 0)
	{
		div += abs(diff);
	} 
	std::reverse(vecl.begin(), vecl.end());
	while (div--)
	{
		vecl.push_back(0);
	}
	std::reverse(vecl.begin(), vecl.end());
	unsigned int  flag = 0;
	for (int i = vecl.size() - 1, j = vecr.size() - 1; i > -1; --i, --j)
	{
		size_t temp = j < 0 ? 0 : vecr[j];
		vecl[i] = vecl[i] + temp + flag;
		if (vecl[i] > 2)
		{
			vecl[i] = 1;
			flag = 1;
		}
		else if(vecl[i] == 2)
		{
			vecl[i] = 0;
			flag = 1;
		}
		else if(vecl[i] == 1)
		{
			vecl[i] = 1;
			flag = 0;
		}
		else if(vecl[i] == 0)
		{
			vecl[i] = 0;
			flag = 0;
		}
	}
}
//"-"
void bit_sub(std::vector<unsigned int> &vecl, std::vector<unsigned int> &vecr)
{
	unsigned int div = 0;
	int diff = ((unsigned int)vecl.size() - (unsigned int)vecr.size());
	if (diff < 0)
	{
		div += abs(diff);
	} 
	std::reverse(vecl.begin(), vecl.end());
	while (div--)
	{
		vecl.push_back(0);
	}
	std::reverse(vecl.begin(), vecl.end());
	int  flag = 0;
	for (int i = vecl.size() - 1, j = vecr.size() - 1; i > -1; --i, --j)
	{
		size_t temp = j < 0 ? 0 : vecr[j];
		vecl[i] = vecl[i] - temp - flag;
		if(vecl[i] < 0)
		{
			vecl[i] = 1;
			flag = 1;
		}
		else
		{
			flag = 0;
		}
	}
}
//"*"
void bit_mult(std::vector<unsigned int> &vecl, std::vector<unsigned int> &vecr)
{
	std::vector<unsigned int> tempv(vecl);
	unsigned int length = vecl.size() + vecr.size() + 1;
	vecl.clear();
	std::reverse(vecl.begin(), vecl.end());
	while (length--)
	{
		vecl.push_back(0);
	}
	std::reverse(vecl.begin(), vecl.end());
	unsigned int count  = 0;
	std::vector<unsigned int> temp_left={0}, temp_right={0};
	for (int i = vecr.size() - 1; i > -1; --i)
	{
		temp_left.clear();
		temp_left = temp_right;
		temp_right.clear();
		for (int j = tempv.size() - 1; j > -1; --j)
		{
			temp_right.push_back(tempv[j] * vecr[i]);
		}
		unsigned int y = count;
		std::reverse(temp_right.begin(), temp_right.end());
		while (y)
		{
			temp_right.push_back(0);
			--y;
		}
		bit_sub(temp_right, temp_left);
		++count;
	}
	vecl = temp_right;
}

unsigned long long int vec_to_int(std::vector<unsigned int> &vec)
{
	int st = 0, res = 0;
	for_each(vec.rbegin(), vec.rend(), [&res, &st](int n){
		res += (n * pow(2, st)); 
		++st;
	});
	return res;
}
*/

void int_to_vec(unsigned long long int initVal, std::vector<unsigned int> &vec)
{
	vec.clear();
	while(initVal > 0)
	{
		initVal % 2 == 0 ? vec.push_back(0) : vec.push_back(1);
		initVal /= 2;
	}
}

class BitArray{
public:
	BitArray (unsigned int initVal) : length(32)//инициализирует биты числом initVal
	{
		int_to_vec(initVal, vec);
		int delta =  32 - this->vec.size();
		std::reverse(this->vec.begin(), this->vec.end());
		while (delta--)
			this->vec.push_back(0);
		std::reverse(this->vec.begin(), this->vec.end());
	}

	BitArray(BitArray& b)
	{
		this->length = b.length;
		this->vec.clear();
		for (int i = 0; i < b.length; ++i)
		{
			this->vec.push_back(b.vec[i]);
		}
	}

	void SetLength (int len = 32)//изменяет длину массива, len – длина в битах. По умолчанию длина равна 32
	{
		int delta = len - length;
		std::reverse(this->vec.begin(), this->vec.end());
		while (delta--)
			this->vec.push_back(0);
		std::reverse(this->vec.begin(), this->vec.end());
		length = len;
	}

	int GetLength()//возвращает длину массива.
	{
		return length;
	}

	void SetBit (int pos, bool value)//выставляет бит pos в значение value
	{
		vec[pos] = value;
	}

	bool GetBit (int pos)//возвращает значение бита pos
	{
		return (bool)vec[pos];
	}

	BitArray& operator<<(int num) //сдвиг влево
	{
		while (num-- > 0)
		{
			this->vec.erase(this->vec.begin());
			this->vec.push_back(0);
		}
		return *this;
	}

	BitArray& operator>>(int num) //сдвиг вправо
	{
		std::reverse(this->vec.begin(), this->vec.end());
		while (num-- > 0)
		{
			this->vec.erase(this->vec.begin());
			this->vec.push_back(0);
		}
		std::reverse(this->vec.begin(), this->vec.end());
		return *this;
	}

	friend BitArray& operator ~(BitArray &ba)
	{
		for_each(ba.vec.begin(), ba.vec.end(), [](unsigned int &n){ n == 0 ? n = 1 : n = 0;});
		return ba;
	}

	friend BitArray& operator &(BitArray &bal, BitArray &bar)
	{
		std::vector<unsigned int>::reverse_iterator baliter = bal.vec.rbegin();
		std::vector<unsigned int>::reverse_iterator bariter = bar.vec.rbegin();
		while(baliter != bal.vec.rend() && bariter != bar.vec.rend())
		{
			*baliter = (*baliter == 1 && *bariter == 1) ? 1 : 0 ;
			++bariter;
			++baliter;
		}
		return bal;
	}

	friend BitArray& operator |(BitArray &bal, BitArray &bar)
	{
		std::vector<unsigned int>::reverse_iterator baliter = bal.vec.rbegin();
		std::vector<unsigned int>::reverse_iterator bariter = bar.vec.rbegin();
		while(baliter != bal.vec.rend() && bariter != bar.vec.rend())
		{
			*baliter = (*baliter == 0 && *bariter == 0) ? 0 : 1;
			++bariter;
			++baliter;
		}
		return bal;
	}

	BitArray& operator=(BitArray & ba)
	{
		this->length = ba.length;
		this->vec = ba.vec;
		return *this;
	}

	friend std::ostream& operator<< (std::ostream & stream, BitArray & obj)//вывод в поток (при выводе группировать по 8 бит)
	{
		int temp =1;
		for_each(obj.vec.begin(), obj.vec.end(), [&stream, &temp](unsigned int &i){stream << i << (temp % 8 == 0 ? " " : ""); ++temp;});
		stream << " " ;
	}

private:
	unsigned int length;
	std::vector<unsigned int> vec;
};

#endif //BITARRAY_H