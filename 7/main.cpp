#include <iostream>
#include "bitarray.h"

using namespace std;

int main(void)
{
	
	
	BitArray a(0x00ff00ff);
	BitArray b(a);
	a = a << 4;
	cout << "1: " << a << endl;
	cout << "2: " << (a & b) << endl;
	
	b.SetLength(65);

	b = (b << 41) | b;

	cout << "3: " << b << endl;
	cout << "4: " << ~b << endl;
	
	return 0;
}