#ifndef CLASSES_H
#define CLASSES_H

#include <iostream>


class Base;

typedef void (Base::*vFunc)(void);

using namespace std;

class Base
{
public:
	Base()
	{
		vTable[0] = &Base::_Func1;
		vTable[1] = &Base::_Func2;
		vTable[2] = nullptr;
	}
	void Func1() { CallFunc(0); } // просто вызов нужной функции из таблицы
	void Func2() { CallFunc(1); }
	void Func3() { CallFunc(2); }

protected:
	void CallFunc(int nb)
	{
		if(vTable[nb] == nullptr)
		{
			cout << "Pure virtual function call" << endl;
			return;
		}
		(this->*vTable[nb])();
	}
	vFunc  vTable[3]; // указатель на таблицу виртуальных функций
	void _Func1() { cout << "Base::Func1" << endl; }
	void _Func2() { cout << "Base::Func2" << endl; }
	// Func3 будет чисто виртуальной (а нашем случае класс можно будет создать)
};

class Derived: public Base
{
public:
	Derived()
	{
		vTable[0] = (void (Base::*)())&Derived::_Func1;
		vTable[2] = (void (Base::*)())&Derived::_Func3;
	}
protected:
	void _Func1() { cout << "Derived::Func1" << endl; }
	// нету _Func2 – возьмем из Base
	void _Func3() { cout << "Derived::Func3" << endl; }

};


#endif //CLASSES_H