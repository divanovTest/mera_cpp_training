#include <iostream>
#include "Classes.h"

using namespace std;

int main(void)
{
	Base * base = new Derived();

	base->Func1();
	base->Func2();
	base->Func3();
	delete base;
	
	cout << endl;

	base = new Base();
	
	base->Func1();
	base->Func2();
	base->Func3();
	delete base;
	
	return 0;
}