#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <cstdlib>
#include <memory>

template<typename T> class SymmetricMatrix{
public:
	typedef std::unique_ptr<T[]> ptrtemplate;
	SymmetricMatrix(size_t size):dimension(size)
	{
		_array= std::unique_ptr<ptrtemplate[]>(new ptrtemplate[size]);

		for (int i = 0; i < size; ++i)
		{
			_array[i] = ptrtemplate(new T[i]);
			std::unique_ptr<T[]> _temp = std::make_unique<T[]>(i);  
			for(int j = 0; j <= i; ++j)
			{
				_temp[j] = (T)(rand() % 8 + 1);
			}
			_array[i] = std::move(_temp);
		}
	}

	SymmetricMatrix(const SymmetricMatrix& obj)
	{
		this->dimension = obj.dimension;
		_array = std::unique_ptr<ptrtemplate[]>(new ptrtemplate[dimension]);
		for (int i = 0; i < dimension; ++i)
		{
			_array[i] = ptrtemplate(new T[i]);
			std::unique_ptr<T[]> _temp = std::make_unique<T[]>(i);
			for (int j = 0; j <= i; ++j)
			{
				_temp[j] = obj._array[j][i];
			}
			_array[i] = std::move(_temp);
		}
	}

	~SymmetricMatrix()
	{
	}

	void operator=(const SymmetricMatrix& obj)
	{
		bool ifdiff = false;
		if (obj.dimension != this->dimension)
		{
			this->dimension = obj.dimension;
			_array = std::unique_ptr<ptrtemplate[]>(new ptrtemplate[dimension]);
			ifdiff = true;
		}
		for (int j = 0; j < dimension; ++j)
		{
			if (ifdiff)_array[j] = ptrtemplate(new T[j]);
			std::unique_ptr<T[]> _temp = std::make_unique<T[]>(j);
			for (int i = 0; i <= j; ++i)
			{
				_temp[i] = obj._array[j][i];
			}
			_array[j] = std::move(_temp);
		}
	}

	int* operator()(const int y, const int x)const
	{
		return _array[y][x] != 0 ? &(_array[y][x]) : &(_array[x][y]);
	}

	friend std::ostream& operator << (std::ostream& os, const SymmetricMatrix& obj)
	{
		for (int i = 0; i < obj.dimension; ++i)
		{
			for (int j = 0; j < obj.dimension; ++j)
			{
				os << (*obj(i,j) > 9 ? "" : " ") << *obj(i,j) << " ";
			}
			os << "\n";
		}
		return os;
	}
private:
	std::unique_ptr<ptrtemplate[]> _array;
	size_t dimension;
};

#endif //MATRIX_H