#include <iostream>
#include "Iterator.h"

using namespace std;

int main(void)
{

	List<int> list;
	list.Add(1);
	list.Add(3);
	list.Add(5);

	list.Insert(2, 1);
	list.Insert(4, 3);
	
	list.Remove(0);

	//Iterator<int>& fwdIter = 
	ForwardIterator<int> it(list);
	//Iterator<int>& revIter = 
	ReverseIterator<int> rit(list);

	list.Add(6);

	
	while(it.Next())
	{
		cout << it.Key() << " ";
	}
	cout << endl;	
	
	while(rit.Next())
	{
		cout << rit.Key() << " ";
	}
	cout << endl;
	

	try
	{
		it.Next();
		cout << "Incorrect iterator behavior" << endl;
	}
	catch (IteratorException & e)
	{
		cout << e.Message() << endl;
	}

	try
	{
		it.Reset();
		it.Key();
		cout << "Incorrect iterator behavior" << endl;
	}
	catch (IteratorException & e)
	{
		cout << e.Message() << endl;
	}

	return 0;
}