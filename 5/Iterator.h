#ifndef ITERATOR_H
#define ITERATOR_H

#include <iostream>
#include "../4/List.h"

class IteratorException
{
public:
	virtual const char * Message() = 0;
};

template <class T>
class Iterator
{
public:

	// Переходим к следующему элементу
	virtual bool Next() throw (IteratorException) = 0;
	
	// Возвращаем ссылку на элемент
	virtual T& Key() throw (IteratorException) = 0;
	
	// Возвращаемся в начало списка
	virtual void Reset() = 0; 
};

class IteratorUndefException: public IteratorException
{
public:
	const char * Message()
	{
		return "Iterator is not initialized";
	}
};

class IteratorBoundsException: public IteratorException
{
public:
	const char * Message()
	{
		return "Iterator position is out of bounds";
	}
};

template<typename T> class ForwardIterator : public Iterator<T>
{
public:
	ForwardIterator() : iterator(nullptr)
	{
	}

	ForwardIterator(List<T> &l) : iterator(nullptr)
	{
		list = &l;
	}

	bool Next() throw (IteratorBoundsException)
	{
		
		if (iterator == nullptr)
		{
			iterator = list->head;
		}
		else
		{
			if (iterator->next == nullptr && !count)
			{
				count = true;
				return false;
			}
			else if(iterator->next == nullptr)
			{
				IteratorBoundsException exc;
				throw exc;
			}
			else
			{
				iterator = iterator->next;
			}
		}
		return true;
	}

	T& Key() throw (IteratorException)
	{
		return iterator->data;
	}

	void Reset()
	{
		iterator = nullptr;
	}

private:
	bool count;
	ListElem<T>* iterator;
	List<T>* list;
};

template<typename T> class ReverseIterator : public Iterator<T>
{
public:
	ReverseIterator() : iterator(nullptr), count(false)
	{
	}

	ReverseIterator(List<T> &l) : iterator(nullptr), count(false)
	{
		list = &l;
	}

	bool Next() throw (IteratorBoundsException)
	{
		
		if (iterator == nullptr)
		{
			iterator = list->tail;
		}
		else
		{
			if (iterator->prev == nullptr & !count)
			{
				count = true;
				return false;
			}
			else if(iterator->prev == nullptr)
			{
				IteratorBoundsException exc;
				throw exc;
			}
			else
			{
				iterator = iterator->prev;
			}
		}
		return true;
	}

	T& Key() throw (IteratorException)
	{
		return iterator->data;
	}

	void Reset()
	{
		iterator = nullptr;
	}
private:
	bool count;
	ListElem<T>* iterator;
	List<T>* list;
};

#endif //ITERATOR_H